import json
import cv2
import numpy as np

f = open('output-test.json')
data = json.load(f)

boxCoord = [int(coord) for coord in data[0]['box']]

maskCoord = [int(coord) for coord in data[0]['mask'][0]]

maskCoord = np.array(maskCoord).reshape(-1, 2)

image = cv2.imread('test_image.webp')

image = cv2.cvtColor(image, cv2.COLOR_BGR2BGRA)

# creating a mask of that has the same dimensions of the image
# where each pixel is valued at 0
mask = np.zeros(image.shape[:2], dtype="uint8")

# creating a rectangle on the mask
# where the pixels are valued at 255

#cv2.rectangle(mask, tuple(c for c in boxCoord[0:2]), tuple(c for c in boxCoord[2:4]), 255, -1)

cv2.fillPoly(mask, [maskCoord], 255)

masked = cv2.bitwise_and(image, image, mask=mask)

cv2.imshow("Masked", masked)
cv2.waitKey(0)

cv2.imwrite("img_mask.png", masked)