import cv2
from modules import *
import sqlite3
import os
import json


directories = []


baseDir = '/media/lvh/BELGA-WORK/'
for rep in os.listdir(baseDir):
    if rep[0:2] == '20':
        directories.append(rep)

directories.sort(reverse=True)
print(directories)






parser = Parser()
tb = Turbine()
db = DB()

try:

    for rep in directories:
        directory = '/media/lvh/BELGA-WORK/%s' % rep
        for filename in os.listdir(directory):
            if filename.endswith(".xml"):
                
                file = os.path.join(directory, filename)

                
                #print "opening %s" % file
                print("####SCANNING %s" % file)
                
                
                newsData = parser.parse(file)
                print(newsData)
                
                

                if('image' in newsData):
                    newsItemId = db.insertNewsItem(newsData, os.path.join(rep, newsData['image']))
                    imagePath = os.path.join(directory, newsData['image'])
                    objData = {}
                    print("##############DETECTRON TURBINE ON %s" %newsData['image'])
                    im = cv2.imread(imagePath)
                    preds = tb.predict(im)
                    for obj in preds:
                        objData['label']  = obj['label']
                        objData['box'] = json.dumps(obj['box'],  default=tb.jsondefault)
                        objData['score'] = obj['score']
                        objData['mask'] = json.dumps(obj['mask'], default=tb.jsondefault)
                        print(objData)
                        db.insertObjItem(objData, newsItemId)

                db.commit()
                print("###DONE!")
                
        



except KeyboardInterrupt:
    print("keyboard interrupt")
    exit()


'''
def dbStructure():
    cur.execute("CREATE TABLE ...")

con = sqlite3.connect()
cur = con.cursor()
'''





'''



tb = Turbine()


im = cv2.imread("98111181_96760204.jpg")
(preds, im2) = tb.compute(im)
#print(preds)
cv2.imwrite('output-test.png', im2)
file = open('output-test.json', 'w')
file.write(preds) 
file.close()
'''