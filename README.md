# belga-detectron

## detect.py
Analyse pictures from the Belga newsfeed archive to perform object detection and image segmentation with detectron2. The analysis is saved to a database for further processing.