import lxml.etree as ET
import re
import os
import datetime

class Parser:
    def __init__(self):
        print('parser init')
        self.lparser = ET.XMLParser(recover=True)

    def parse(self, file):

        output = {}

        tree = ET.parse(file, parser=self.lparser)

        for d in tree.xpath("//NewsEnvelope/DateAndTime/text()"):
            output['datetime'] = datetime.datetime.strptime(d, "%Y%m%dT%H%M%S")

        for item in tree.iter("NewsComponent"):
            roleTag = item.find("Role")
            if roleTag != None:
                if roleTag.get('FormalName') == "Caption":
                    for r in item.xpath(".//ContentItem/DataContent/text()"):
                        output['caption'] = r 
                elif roleTag.get('FormalName') == "Image":
                    for r in item.xpath(".//ContentItem/@Href"):
                        output['image'] = r
        
        return output