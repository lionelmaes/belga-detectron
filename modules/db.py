import sqlite3


class DB:
    def __init__(self):
        print('DB init')
        self.conn = sqlite3.connect('belga-detectron.db')

    def createDB(self):
        conn = sqlite3.connect('belga-detectron.db')
        conn.execute('''CREATE TABLE NEWSITEM
         (ID INTEGER PRIMARY KEY AUTOINCREMENT,
         IMAGE           TEXT    NOT NULL,
         DATETIME        TIMESTAMP     NOT NULL,
         CAPTION         TEXT);''')
        
        conn.execute('''CREATE TABLE OBJECT
         (ID INTEGER PRIMARY KEY AUTOINCREMENT,
         NEWSITEM        INT    NOT NULL,
         LABEL           TEXT     NOT NULL,
         SCORE            INT   NOT NULL,
         BOX            TEXT    NOT NULL,
         MASK             TEXT NOT NULL);''')

        print("Tables created successfully");



        conn.close()


    def insertNewsItem(self, newsData, imagePath):
        cursor=self.conn.cursor()
        if not(caption in newsData)
            newsData['caption'] = ''
            print("no caption?")
            
        cursor.execute('INSERT INTO NEWSITEM (IMAGE, DATETIME, CAPTION) VALUES (?,?,?)',
            (imagePath,newsData['datetime'],newsData['caption']))
        
        print(cursor.lastrowid)
        return cursor.lastrowid


    def insertObjItem(self, objData, newsItem):
        cursor=self.conn.cursor()
        
        cursor.execute('INSERT INTO OBJECT (NEWSITEM, LABEL, SCORE, BOX, MASK) VALUES (?,?,?,?,?)',
            (newsItem,objData['label'],objData['score'].strip('%'),objData['box'],objData['mask']))
    
    def commit(self):
        self.conn.commit()

'''
db = DB()
db.createDB()
'''